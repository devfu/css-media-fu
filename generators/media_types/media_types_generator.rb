class MediaTypesGenerator < Rails::Generator::Base
  def manifest
    record do |m|
 			m.file 'default_media_types.yml', 'config/media_types.yml'
    end
  end
end