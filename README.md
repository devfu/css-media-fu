# Css Media Fu

- This code is abandonware. Use at your own risk.

Based off of a similar PHP solution found at http://mezzoblue.com/archives/2008/03/18/mediatyping/

This plugin should make it easier to detect which browser is being used to view the site and
display the appropriate stylesheet.

Browser detection via the user agent string is *not* reliable, as many browsers allow this to be
changed easily.

## Installation

To install the default media_types.yml run script/generate media_types

## Example

The plugin expects to find a list of known media types in config/media_types.yml. Use the default (again, 
taken from http://mezzoblue.com/archives/2008/03/18/mediatyping/) by running the generator, or roll your
own. 

The format is 

```yaml
:media:
- STRING
```

where media is the value that will be returned if the user agent contains STRING.

The plugin is available in all of your views/helpers, so you can use the following code to load
media specific stylesheets:

`<%= stylesheet_link_tag “custom_#{media?}_specific” %>`

It is possible to override the detected stylesheet, for example to override with a URL parameter:
	
`<% set_media! params[:media] if params[:media] %>`
