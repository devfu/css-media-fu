# CSS Media Fu
 
# This plugin parses browser user agent
# and sets the session[:media] variable
# according to a look up table in 
# config/media_types.yml

module CSS_Media_fu

	# Load the config file, and search for a match to the given string
	def find_media_type(agent)
		media_types = YAML::load(File.open("#{RAILS_ROOT}/config/media_types.yml"))
		
		media = "screen"
		media_types.each {|key, value| value.each {|v| media = "#{key}" if agent.include?(v)}}
		return media
	end

	# Checks for session[:media] and if it exists, returns it. 
	# Otherwise, find media_type based on the browser user agent string.
	def set_media
		session[:media] = find_media_type(request.env['HTTP_USER_AGENT'])  unless session[:media]
		return session[:media]
	end

	# Explicitly set the media session variable
	def set_media!(media)
		session[:media] = media
	end

	# Returns the current session media variable, or if it doesn't exist, create it
	def media?
		session[:media] ? session[:media] : set_media
	end
	
	# Used to clear the session variable.
	def reset_media
		session[:media] = nil
	end
end